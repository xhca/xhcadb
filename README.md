# xhcaDB

A simple application that uses a postgreSQL database containing XHCA data


# High-level Objectives

To properly frame the queries and schema of this database, we explicitly list our high-level
objectives here:

    1. We want to enable queries on cells, genes, and "groups"; where a group is a general term for
       clusters, cell types, gene families, etc.
    2. Queries on cells should always be done over a set of cells (all, within a group, or across a
       set of groups).
    3. Queries on genes may be over an ad-hoc set of genes, or may be over a set of genes (all,
       within a group, or across a set of groups).
    4. Queries may be at the expression level (process expression values/counts)
    5. Queries may be at the group level (process groups of cells or genes without inspecting
       expression values).
    6. Queries may specify quantitative expression thresholds or qualitative expression thresholds.

For reference, here are some example queries expressed in natural language:

    * What genes are expressed in both `b-cells` and `t-cells`?
    * What cells express genes `PRR5`, `ZFP69B`, ..., and `GAL` and what are their cell types?
    * What cells may develop into `b-cells` and `t-cells` and express genes `PRR5`, ..., and `GAL`?

# Implemented Queries

Here, we describe the queries we have implemented that illustrate the functionality we want to
research.

## Use Case 1 (select)

`SELECT` cells `WHERE` cells.cluster.type = `cluster 1`


## Use Case 2 (select-filter)

### Cells

`SELECT` cells `WHERE` cells.expresses(gene\_set)

### Cell Types

`SELECT` cells `WHERE` cells.cluster.expresses(gene\_set)


## Use Case 3 (select-filter-project)

Note: in the below example, `gene_set_1` may or may not be the same as `gene_set_2`.

`SELECT` cells
`WHERE` cells.cluster.expresses(gene\_set\_1)
`AND`   cells.expresses(gene\_set\_2)
