"""
First-class types supported in the database. Also known as primary entities.
"""

# ------------------------------
# Imports
from collections import namedtuple

from owlready import onto_path, get_ontology


# ------------------------------
# Module-level variables

ExpressionTuple = namedtuple(
    'ExpressionTuple',
    ['cell_id', 'gene_name', 'value']
)



# ------------------------------
# Classes
class Cell(object):
    __slots__ = ('cell_id',)

    def __init__(self, id, **kwargs):
        super().__init__(**kwargs)

        self.cell_id = id


class Gene(object):
    __slots__ = ('gene_name',)

    def __init__(self, name, **kwargs):
        super().__init__(**kwargs)

        self.gene_name = name


class Expression(ExpressionTuple): pass

class GeneSet(set):  pass

class CellOntology(object):
    default_onto_path = 'resources/ontologies/cl-base.owl'


class CellType(CellOntology): pass
