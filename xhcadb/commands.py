class QueryCells(object):
    @classmethod
    def by_celltype(cls, cell_type_as_str):
        """
        :cell_type_as_str: is expected to be the name of a cell type as a string.
        """

        pass

    @classmethod
    def by_expression(cls, gene_expr_as_tuples):
        """
        :gene_expr_as_tuples: is expected to be a sequence or collection of `Expression` objects.
        """

        pass
