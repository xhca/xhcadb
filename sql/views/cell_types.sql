CREATE OR REPLACE VIEW celltype_expression
AS SELECT  cluster_methods.cluster_name AS name
          ,cluster_methods.cluster_id   AS id
          ,centroids.centroid_id        AS centroid_id
          ,centroids.gene_name          AS gene_name
          ,centroids.expr_val           AS expr_val

   FROM   cluster_methods
          JOIN centroids
            ON (cluster_methods.centroid_id = centroids.centroid_id)

          JOIN genes
            ON (centroids.gene_name = genes.gene_name)
;
