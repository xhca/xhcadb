CREATE OR REPLACE VIEW geneset_expression
AS SELECT  genesets.geneset_id    AS geneset_id
          ,genesets.geneset_name  AS geneset_name
          ,genesets.geneset_group AS geneset_group
          ,cells.cell_id          AS cell_id
          ,genes.gene_name        AS gene_name
          ,expression.expr_val    AS expr_val

   FROM   genesets
          JOIN geneset_membership
            ON (genesets.geneset_id = geneset_membership.geneset_id)

          JOIN genes
            ON (geneset_membership.gene_name = genes.gene_name)

          JOIN expression
            ON (geneset_membership.gene_name = expression.gene_name)

          JOIN cells
            ON (expression.cell_id = cells.cell_id)
;
