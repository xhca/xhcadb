CREATE OR REPLACE VIEW geneset_view
AS SELECT  genesets.geneset_id    AS geneset_id
          ,genesets.geneset_name  AS geneset_name
          ,genesets.geneset_group AS geneset_group
          ,genes.gene_name        AS gene_name

   FROM   genesets
          JOIN geneset_membership
            ON (genesets.geneset_id = geneset_membership.geneset_id)

          JOIN genes
            ON (geneset_membership.gene_name = genes.gene_name)
;
