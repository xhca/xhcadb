CREATE OR REPLACE VIEW cell_expression
AS SELECT  cluster_methods.cluster_name AS name
          ,cluster_methods.cluster_id   AS id
          ,expression.cell_id           AS cell_id
          ,expression.gene_name         AS gene_name
          ,expression.expr_val          AS expr_val

   FROM   cluster_methods
          JOIN clusters
            ON (cluster_methods.cluster_id = clusters.cluster_id)

          JOIN expression
            ON (clusters.cell_id = expression.cell_id)

          JOIN cells
            ON (clusters.cell_id = cells.cell_id)

          JOIN genes
            ON (expression.gene_name = genes.gene_name)
;
