CREATE OR REPLACE VIEW singlecell_view
AS SELECT  genesets.geneset_name        AS geneset_name
          ,cluster_methods.cluster_name AS cluster_name
          ,expression.gene_name         AS gene_name
          ,centroids.centroid_id        AS centroid_id
          ,centroids.expr_val           AS centroid_expr
          ,expression.cell_id           AS cell_id
          ,expression.expr_val          AS cell_expr

   FROM   genesets
          JOIN geneset_membership gm
            ON (genesets.geneset_id = gm.geneset_id)

          JOIN genes
            ON (gm.gene_name = genes.gene_name)

          JOIN expression
            ON (genes.gene_name = expression.gene_name)

          JOIN cells
            ON (expression.cell_id = cells.cell_id)

          JOIN clusters
            ON (cells.cell_id = clusters.cell_id)

          JOIN cluster_methods
            ON (clusters.cluster_id = cluster_methods.cluster_id)

          LEFT JOIN centroids
                 ON (cluster_methods.centroid_id = centroids.centroid_id)
;
