-- ------------------------------
-- Drop foreign keys
ALTER TABLE IF EXISTS expression DROP CONSTRAINT expression_cell_id_fkey;
ALTER TABLE IF EXISTS expression DROP CONSTRAINT expression_gene_name_fkey;

ALTER TABLE IF EXISTS clusters DROP CONSTRAINT clusters_cell_id_fkey;
ALTER TABLE IF EXISTS centroids DROP CONSTRAINT centroids_centroid_id_fkey;
ALTER TABLE IF EXISTS centroids DROP CONSTRAINT centroids_gene_name_fkey;

ALTER TABLE IF EXISTS geneset_membership DROP CONSTRAINT geneset_membership_geneset_id_fkey;
ALTER TABLE IF EXISTS geneset_membership DROP CONSTRAINT geneset_membership_gene_name_fkey;
