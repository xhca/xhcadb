-- ------------------------------
-- Alter statements (primary and foreign keys, etc.)
ALTER TABLE IF EXISTS expression ADD FOREIGN KEY (cell_id)   REFERENCES cells (cell_id);
ALTER TABLE IF EXISTS expression ADD FOREIGN KEY (gene_name) REFERENCES genes (gene_name);

ALTER TABLE IF EXISTS clusters   ADD FOREIGN KEY (cell_id)     REFERENCES cells           (cell_id);
ALTER TABLE IF EXISTS centroids  ADD FOREIGN KEY (centroid_id) REFERENCES cluster_methods (centroid_id);
ALTER TABLE IF EXISTS centroids  ADD FOREIGN KEY (gene_name)   REFERENCES genes           (gene_name);

ALTER TABLE IF EXISTS geneset_membership ADD FOREIGN KEY (geneset_id) REFERENCES genesets (geneset_id);
ALTER TABLE IF EXISTS geneset_membership ADD FOREIGN KEY (gene_name)  REFERENCES genes    (gene_name);

-- ------------------------------
-- Create Indices for performance
CREATE INDEX IF NOT EXISTS expr_index           ON expression(gene_name, expr_val);
CREATE INDEX IF NOT EXISTS centroid_index       ON centroids(gene_name, expr_val);
CREATE INDEX IF NOT EXISTS geneset_index        ON genesets(geneset_id, geneset_name);
CREATE INDEX IF NOT EXISTS geneset_member_index ON geneset_membership(gene_name);
