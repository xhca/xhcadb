-- Change username as appropriate

-- ------------------------------
-- Create users with CREATEDB privilege
CREATE USER akmontan         WITH CREATEDB;
CREATE USER disorderly_guest WITH CREATEDB;


-- ------------------------------
-- Create databases
CREATE DATABASE xhca WITH OWNER = akmontan;


-- ------------------------------
-- Grant privileges to each user

-- admin privileges
GRANT ALL
   ON DATABASE xhca
   TO  akmontan
      ,disorderly_guest
;

-- privileges to access database tables
GRANT ALL
   ON ALL TABLES IN SCHEMA public
   TO  akmontan
      ,disorderly_guest
;

-- privileges to access local files (for the `COPY` command)
GRANT  pg_read_server_files
   TO  akmontan
      ,disorderly_guest
 WITH ADMIN OPTION
;
