-- ------------------------------ 
-- Views for groups (cell types, gene families)
CREATE OR REPLACE VIEW celltype_expression
AS SELECT  cluster_methods.cluster_name AS name
          ,cluster_methods.cluster_id   AS id
          ,centroids.centroid_id        AS centroid_id
          ,centroids.gene_name          AS gene_name
          ,centroids.expr_val           AS expr_val

   FROM   cluster_methods
          JOIN centroids
            ON (cluster_methods.centroid_id = centroids.centroid_id)
;

CREATE OR REPLACE VIEW cell_expression
AS SELECT  cluster_methods.cluster_name AS name
          ,cluster_methods.cluster_id   AS id
          ,expression.cell_id           AS cell_id
          ,expression.gene_name         AS gene_name
          ,expression.expr_val          AS expr_val

   FROM   cluster_methods
          JOIN clusters
            ON (cluster_methods.cluster_id = clusters.cluster_id)

          JOIN expression
            ON (clusters.cell_id = expression.cell_id)
;
