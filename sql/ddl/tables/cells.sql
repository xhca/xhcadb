-- ------------------------------
-- primitive entity (logical data)

CREATE TABLE IF NOT EXISTS cells (
     cell_id VARCHAR(48)

    ,PRIMARY KEY (cell_id)
);
