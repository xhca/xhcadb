CREATE TABLE IF NOT EXISTS centroids (
     centroid_id BIGINT
    ,gene_name   VARCHAR(32)
    ,expr_val    SMALLINT

    ,PRIMARY KEY (centroid_id, gene_name)
    ,FOREIGN KEY (centroid_id) REFERENCES cluster_methods (centroid_id)
    ,FOREIGN KEY (gene_name)   REFERENCES genes           (gene_name)
);
