-- ------------------------------
-- core entity (experimental data)

-- default partition strategy (cell_id)
CREATE TABLE IF NOT EXISTS expression (
     cell_id   VARCHAR(48)
    ,gene_name VARCHAR(32)
    ,expr_val  SMALLINT

) PARTITION BY RANGE (cell_id);


-- alternate partitioning (gene_name)
CREATE TABLE IF NOT EXISTS expression_genepartitioned (
     cell_id   VARCHAR(48)
    ,gene_name VARCHAR(32)
    ,expr_val  SMALLINT

) PARTITION BY LIST (gene_name);
