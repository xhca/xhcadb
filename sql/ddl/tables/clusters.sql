-- ------------------------------
-- clusters (analytical entities)

CREATE TABLE IF NOT EXISTS clusters (
     cluster_id BIGINT
    ,cell_id    VARCHAR(48)

    ,PRIMARY KEY (cluster_id, cell_id)
);
