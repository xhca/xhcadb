-- ------------------------------
-- primitive entity (logical data)

CREATE TABLE IF NOT EXISTS genes (
     gene_name VARCHAR(32)

    ,PRIMARY KEY (gene_name)
);
