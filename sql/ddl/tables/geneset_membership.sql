-- ------------------------------
-- Bridge between tables "genesets" and "genes"

CREATE TABLE IF NOT EXISTS geneset_membership (
     geneset_id   BIGINT
    ,gene_name    VARCHAR(32)

    ,PRIMARY KEY (geneset_id, gene_name)
    -- ,FOREIGN KEY (geneset_id) REFERENCES genesets (geneset_id)
    -- ,FOREIGN KEY (gene_name)  REFERENCES genes    (gene_name)
);
