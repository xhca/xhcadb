-- EXPLAIN ANALYZE

SELECT  geneset_id
       ,gene_stats.gene_name     AS gene_name
       ,gene_stats.stddev_bygene AS stddev_bygene
       ,gene_stats.avg_bygene    AS avg_bygene

FROM   (SELECT    gene_name        AS gene_name
                 ,STDDEV(expr_val) AS stddev_bygene
                 ,AVG(expr_val)    AS avg_bygene
        FROM     expression AS expr
        GROUP BY gene_name
       ) gene_stats

       JOIN geneset_membership gsm
         ON (gsm.gene_name = gene_stats.gene_name)
;
