-- EXPLAIN -- ANALYZE

-- Create a temporary table to get the size of the result set
-- Expected size:
--  - 32  bytes (varchar)
--  - 32  bytes (varchar)
--  - 32  bytes (varchar)
--  - 100 bytes (varchar)
--  - 32  bytes (varchar)
--  - 16  bytes (varchar) (sum: 244)
--  -  2  bytes (smallint) or 5 - 10 bytes (numeric) (sum: 246 - 254)
-- CREATE TEMP TABLE tmp_moderate_query AS

SELECT    cluster_name AS cluster_name
         ,method_name  AS cluster_method
         ,geneset_name AS geneset
         ,cell_id      AS cell_id
         ,gene_name    AS gene_name
         ,expr_val     AS expr_val

-- SELECT   COUNT(*)

FROM     expression expr

          JOIN geneset_membership gsm
         USING (gene_name)

          JOIN (SELECT    gene_name        AS gene_name
                         ,STDDEV(expr_val) AS stddev_bygene
                         ,AVG(expr_val)    AS avg_bygene
                FROM     expression AS expr
                GROUP BY gene_name
               ) gene_stats
         USING (gene_name)

         --  JOIN (SELECT    geneset_id
         --                  -- ,geneset_group
         --                  -- ,geneset_name

         --        FROM       genesets
         --                    JOIN geneset_membership
         --                   USING (geneset_id)

         --        GROUP BY   geneset_id
         --                  -- ,geneset_group
         --        HAVING       (geneset_group = 'msigdb-v7.2-C7'    AND count(*) BETWEEN 190 AND 210)
         --                  OR (geneset_group = 'msgidb-v7.2-C5-bp' AND count(*) BETWEEN 50  AND 100)
         --       ) bp_genesets
         -- USING (geneset_id)

          JOIN clusters
         USING (cell_id)

          JOIN cluster_methods
         USING (cluster_id)

WHERE       cluster_name IN ( '1_1_2_2_3_1'
                             ,'1_2_2_3_2_2'
                             ,'1_1_1_1_4_3'
                             ,'1_1_1_3_2_1'
                            )
        AND geneset_id IN (SELECT    geneset_id
                           FROM      genesets
                                      JOIN geneset_membership
                                     USING (geneset_id)

                           GROUP BY  geneset_id
                           HAVING       (geneset_group = 'msigdb-v7.2-C7'    AND count(*) BETWEEN 190 AND 210)
                                     OR (geneset_group = 'msgidb-v7.2-C5-bp' AND count(*) BETWEEN 50  AND 100)
                          )
-- LIMIT 100
;
