-- EXPLAIN ANALYZE
-- SELECT    cell_id       AS cell_id
--          ,gene_name     AS gene_name
--          ,stddev_bygene AS stddev_bygene
--          ,avg_bygene    AS avg_bygene
--          ,geneset_name  AS geneset_name
SELECT   count(*)

FROM     expression expr

          JOIN geneset_membership gsm
         USING (gene_name)

          JOIN (SELECT    gene_name        AS gene_name
                         ,STDDEV(expr_val) AS stddev_bygene
                         ,AVG(expr_val)    AS avg_bygene
                FROM     expression AS expr
                GROUP BY gene_name
               ) gene_stats
         USING (gene_name)

          JOIN (SELECT     geneset_group
                          ,geneset_id
                          ,geneset_name

                FROM       genesets
                            JOIN geneset_membership
                           USING (geneset_id)

                GROUP BY  geneset_group, geneset_id
                HAVING       (geneset_group = 'msigdb-v7.2-C7'    AND count(*) BETWEEN 190 AND 210)
                          OR (geneset_group = 'msgidb-v7.2-C5-bp' AND count(*) BETWEEN 50  AND 100)
               ) bp_genesets
         USING (geneset_id)
;
