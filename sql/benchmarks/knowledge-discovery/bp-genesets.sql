-- SELECT     bp_genesets.geneset_name  AS geneset_name
--           ,bp_genesets.geneset_group AS group_name
--           ,gsm.gene_name             AS gene_name

SELECT COUNT(*)

FROM      geneset_membership gsm
          JOIN (SELECT     geneset_id
                          -- ,geneset_group
                          -- ,geneset_name

                FROM      genesets
                           JOIN geneset_membership
                          USING (geneset_id)

                GROUP BY   geneset_id
                          -- ,geneset_group
                HAVING       (geneset_group = 'msigdb-v7.2-C7'    AND count(*) BETWEEN 190 AND 210)
                          OR (geneset_group = 'msgidb-v7.2-C5-bp' AND count(*) BETWEEN 50  AND 100)
               ) bp_genesets
            ON (gsm.geneset_id = bp_genesets.geneset_id)
;
