SELECT    cluster_methods.cluster_name AS cluster_name
         ,cluster_methods.method_name  AS cluster_method
         ,expr.cell_id                 AS cell_id
         ,gene_stats.gene_name         AS gene_name

FROM     expression expr

         JOIN clusters
           ON (expr.cell_id = clusters.cell_id)

         JOIN cluster_methods
           ON (clusters.cluster_id = cluster_methods.cluster_id)

         JOIN (SELECT    gene_name        AS gene_name
                        ,STDDEV(expr_val) AS stddev_bygene
                        ,AVG(expr_val)    AS avg_bygene
               FROM     expression
               GROUP BY gene_name
              ) gene_stats
           ON (    expr.gene_name = gene_stats.gene_name
               AND expr.expr_val >= (gene_stats.avg_bygene + gene_stats.stddev_bygene)
              )

WHERE    gene_stats.gene_name IN (SELECT    gsm.gene_name
                                  FROM      genesets gs
                                            JOIN geneset_membership gsm
                                              ON (gs.geneset_id = gsm.geneset_id)
                                  GROUP BY  gs.geneset_id, gs.geneset_group, gsm.gene_name
                                  HAVING       (gs.geneset_group = 'msigdb-v7.2-C7'    AND count(*) BETWEEN 190 AND 210)
                                            OR (gs.geneset_group = 'msgidb-v7.2-C5-bp' AND count(*) BETWEEN 50  AND 100)
                                 )
LIMIT 100
;
