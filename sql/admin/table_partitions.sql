-- SELECT     count(*) AS partition_count
SELECT     relname                               AS table_name
FROM       pg_class c
LEFT JOIN  pg_namespace n ON n.oid = c.relnamespace
    WHERE      relkind = 'r'
           AND relname LIKE 'expression_1%'
-- LIMIT 10
;
