# ------------------------------
# Glossary

# aREA -> analytic    Rank-based Enrichment Analysis
# wREA -> weighted(?) Rank-based Enrichment Analysis

#' @details
#' A function that initializes the enrichment set as an [N x 1] matrix if provided as a 1-D array,
#' or returns as is if provided as an [N x M] matrix.
#'
#' @param sigs_as_list_or_matrix
#' enrichment signature as a list or a matrix. If a matrix, return as-is. If a list, convert it to
#' a single-column matrix.
#' 
#' @return
#' An [N x M] matrix, containing N traits (features) and M signatures. That is, each
#' signature is a column, and every row represents a particular feature of each signature.

matrix_from_signatures <- function(sigs_as_list_or_matrix) {
    if (!is.matrix(sigs_as_list_or_matrix) or is.data.frame(sigs_as_list_or_matrix)) {
        sigs_as_list_or_matrix <- matrix(
             data=sigs_as_list_or_matrix
            ,nrow=length(sigs_as_list_or_matrix)
            ,ncol=1
            ,byrow=FALSE
            ,dimnames=list(names(sigs_as_list_or_matrix))
        )
    }

    return (sigs_as_list_or_matrix)
}
