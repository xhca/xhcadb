#!/usr/bin/bash


# ------------------------------
# Overview

# This script expects to be provided a directory with MTX files, but in a flat hierarchy, where
# each MTX file is in the top-level directory. Then, this script determines the proper
# directory name for each matrix to be a file matching the given pattern, with that pattern
# removed. In the case of the matrix data, files in the directory with the suffix ".mtx" should be
# moved to a sub-directory with the same name as the prefix (everything before ".mtx").
#
# As of this writing, cell metadata files have the suffix "_cells.csv" and gene metadata files have
# the suffix "_genes.csv", and are moved and renamed to "cells.tsv" and "genes.tsv", respectively.
#
# As an example, for a hypothetical directory `mtx-matrices/`:
#
#   1_4_9_4_3_1000000.mtx       -> 1_4_9_4_3_1000000/matrix.mtx
#   1_4_9_4_3_1000000_cells.csv -> 1_4_9_4_3_1000000/cells.tsv
#   1_4_9_4_3_1000000_gene.csv  -> 1_4_9_4_3_1000000/genes.tsv


# ------------------------------
# Global variables and Command-line Arguments

# This is a directory containing matrices in MTX format, but with the files not properly in their
# sub directories. `/path/to/5000-cells` should contain files, `1_1_1_1_1_1.mtx`,
# `1_1_1_1_1_1_cells.csv`, `1_1_1_1_1_1_genes.csv`, etc.
path_to_mtx_matrices="${1:?"Please provide a directory path containing MTX-formatted matrices"}"


# ------------------------------
# Functions

function mv_matrix_files() {
    search_dir="${1:?"Please provide the root directory of the MTX files"}"
    file_suffix="${2:?"Please provide a suffix to match"}"
    new_filename="${3:?"Please provide a new file name to use"}"

    matched_files=($(find "${search_dir}" -mindepth 1 -maxdepth 1 -type f -name "*${file_suffix}"))

    echo "Matched [${#matched_files[@]}] files..."
    echo "${matched_files[0]}"
    echo "${matched_files[1]}"

    for matched_file in ${matched_files[@]}; do
        matrix_name=$(basename "${matched_file}" "${file_suffix}")
        matrix_dir=$(dirname "${matched_file}")
        new_filepath="${matrix_dir}/${matrix_name}/${new_filename}"
    
        echo "Moving: '${matched_file}' -> '${matrix_dir}/${matrix_name}'"
        [[ ! -d "${matrix_dir}/${matrix_name}" ]] && mkdir -p "${matrix_dir}/${matrix_name}"

        echo "mv '${matched_file}' '${matrix_dir}/${matrix_name}/${new_filename}'"
        mv "${matched_file}" "${matrix_dir}/${matrix_name}/${new_filename}"
    done
}


# ------------------------------
# Main logic

# Move the matrix file
mv_matrix_files "${path_to_mtx_matrices}" ".mtx"      "matrix.mtx"

# Move the cell metadata file
mv_matrix_files "${path_to_mtx_matrices}" "_cell.csv" "cells.tsv"

# Move the cell metadata file
mv_matrix_files "${path_to_mtx_matrices}" "_gene.csv" "genes.tsv"
