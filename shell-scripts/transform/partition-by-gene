#!/usr/bin/bash


# ------------------------------
# Overview

# This script Take bulkload files and re-partitions them by gene instead of cell clusters.


# ------------------------------
# Global variables and Command-line Arguments

db_name="${1:?"Please provide a database name"}"
path_to_gene_list="${2:?"Please provide path to gene list"}"
path_to_bulkload_root="${3:?"Please provide a directory to write TSV-formatted files"}"

dataset_name=$(basename "${path_to_bulkload_root}")
bulkload_parentdir=$(dirname "${path_to_bulkload_root}")
path_to_gene_partitions="${bulkload_parentdir}/${dataset_name}-genepartitions"
mkdir -p "${path_to_gene_partitions}"

declare -a pid_pool
# num_procs=$(nproc)
num_procs=$(( 4 ))
input_partition_ndx=$(( 0 ))


# ------------------------------
# Validation

if [[ ! -d "${path_to_bulkload_root}" ]]; then
    echo "Please provide a valid output directory for TSV-formatted files"
    exit 1
fi


# ------------------------------
# Main work

gene_list=($(cat "${path_to_gene_list}"))
input_partitions=($(find "${path_to_bulkload_root}" -name '*.tsv'))

while [[ ${input_partition_ndx} -lt ${#input_partitions[@]} ]]; do
    path_to_partition_file="${input_partitions[input_partition_ndx]}"
    input_partition_ndx=$(( ${input_partition_ndx} + 1 ))
    gene_ndx=$(( 0 ))

    while [[ ${gene_ndx} -lt ${#gene_list[@]} ]]; do
        for proc_id in $(seq 1 ${num_procs}); do
            gene_name="${gene_list[gene_ndx]}"
            gene_ndx=$(( ${gene_ndx} + 1 ))

            [[ ${gene_ndx} -ge ${#gene_list[@]} ]] && break

            normalized_gene_name=$(echo "${gene_name}" | sed 's/[/]/_/')
            gene_expr_filepath="${path_to_gene_partitions}/${normalized_gene_name}.tsv"

            grep "${gene_name}" "${path_to_partition_file}" >> "${gene_expr_filepath}" &
            conversion_pid=$!

            echo -e "[${proc_id}] (${conversion_pid}) >>\t'grep ${gene_name}'..."
            pid_pool[${proc_id}]=${conversion_pid}
        done

        for proc_id in $(seq 1 ${num_procs}); do
            wait ${pid_pool[proc_id]}
            echo -e "<<\t [${proc_id}] (${pid_pool[proc_id]}): completed"
        done
    done
done
