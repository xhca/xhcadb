#!/usr/bin/bash


tmptable_size=$(sudo du -cs /mnt/data-experiments/experimentdb/base/pgsql_tmp | cut -f1 | head -1)
# num_iters=0

while [[ -n "$(pgrep psql)" ]]; do
    sleep 10

    new_tmptable_size=$(sudo du -cs /mnt/data-experiments/experimentdb/base/pgsql_tmp | cut -f1 | head -1)
    if [[ ${new_tmptable_size} -gt ${tmptable_size} ]]; then
        # echo ${new_tmptable_size}
        tmptable_size=${new_tmptable_size}
    fi

    # num_iters=$((num_iters + 1))
    # if [[ ${num_iters} -ge 2 ]]; then
    #     break
    # fi
done

echo "$((tmptable_size / 1024 / 1024))G"
