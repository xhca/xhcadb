#!/usr/bin/bash

# ------------------------------
# Global Variables
dbname="xhca"
input_basedir="tests/inputs"
output_basedir="tests/outputs"


# ------------------------------
# Simple select queries

# Cell-level expression; by cluster name
expr_by_cluster=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,cell_id
           ,gene_name
           ,expr_val

      FROM      expression expr
           JOIN clusters        USING (cell_id)
           JOIN cluster_methods USING (cluster_id)

     WHERE cluster_name IN (%s)
    ;
QUERY_TEMPLATE
)

# Cell-level expression; by geneset name
expr_by_geneset=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,cell_id
           ,gene_name
           ,expr_val

      FROM      expression
           JOIN geneset_membership USING (gene_name)
           JOIN genesets           USING (geneset_id)
           JOIN clusters           USING (cell_id)
           JOIN cluster_methods    USING (cluster_id)

     WHERE geneset_name IN (%s)
    ;
QUERY_TEMPLATE
)


# ------------------------------
# Select-filter queries

# Cell-level expression; by genes
expr_by_filter_gene=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,cell_id
           ,gene_name
           ,expr_val

      FROM      expression
           JOIN clusters           USING (cell_id)
           JOIN cluster_methods    USING (cluster_id)

     WHERE cell_id IN (SELECT cell_id
                         FROM expression
                        WHERE (%s)
                      )
    ;
QUERY_TEMPLATE
)

# Cell-level expression; by geneset
expr_by_filter_geneset=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,cell_id
           ,gene_name
           ,expr_val

      FROM       expression
            JOIN geneset_membership USING (gene_name)
            JOIN clusters           USING (cell_id)
            JOIN cluster_methods    USING (cluster_id)

     WHERE geneset_id IN (SELECT    geneset_id

                            FROM         genesets
                                    JOIN geneset_membership USING (geneset_id)

                          GROUP BY  geneset_id
                          HAVING       (geneset_group = 'msigdb-v7.2-C7'    AND count(*) BETWEEN 190 AND 210)
                                    OR (geneset_group = 'msgidb-v7.2-C5-bp' AND count(*) BETWEEN 50  AND 100)
                         )
    ;
QUERY_TEMPLATE
)

# Cell-level expression; by clusters
# TODO
expr_by_filter_cluster=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,cell_id
           ,gene_name
           ,expr_val

      FROM       expression
            JOIN geneset_membership USING (gene_name)
            JOIN clusters           USING (cell_id)
            JOIN cluster_methods    USING (cluster_id)
    ;
QUERY_TEMPLATE
)

# Cluster-level expression; by genes
centroid_by_filter_genes=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,centroid_id
           ,gene_name
           ,expr_val

      FROM      centroids
           JOIN cluster_methods USING (centroid_id)

     WHERE %s
    ;
;
QUERY_TEMPLATE
)

# Cluster-level expression; by genesets
centroid_by_filter_genesets=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,centroid_id
           ,gene_name
           ,expr_val

      FROM      centroids
           JOIN cluster_methods    USING (centroid_id)
           JOIN geneset_membership USING (gene_name)

     WHERE %s
    ;
;
QUERY_TEMPLATE
)


# ------------------------------
# Select-filter-aggregate queries

# Cell-level expression; filtered by genes; filtered by cluster-level expression
expr_by_aggregate_genes=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,cell_id
           ,gene_name
           ,expr_val

      FROM       expression
            JOIN (SELECT    gene_name        AS gene_name
                           ,STDDEV(expr_val) AS stddev_bygene
                           ,AVG(expr_val)    AS avg_bygene
                  FROM     expression
                  GROUP BY gene_name
                 ) gene_stats
           USING (gene_name)

            JOIN clusters
           USING (cell_id)

            JOIN cluster_methods
           USING (cluster_id)

     WHERE expr_val >= (avg_bygene + stddev_bygene)
    ;
QUERY_TEMPLATE
)

# Cell-level expression; filtered by genesets; filtered by cluster-level expression
# TODO
expr_by_aggregate_genestats=$(cat <<QUERY_TEMPLATE
    SELECT  geneset_id
           ,gene_stats.gene_name     AS gene_name
           ,gene_stats.stddev_bygene AS stddev_bygene
           ,gene_stats.avg_bygene    AS avg_bygene

    FROM   (SELECT    gene_name        AS gene_name
                     ,STDDEV(expr_val) AS stddev_bygene
                     ,AVG(expr_val)    AS avg_bygene
            FROM     expression AS expr
            GROUP BY gene_name
           ) gene_stats

           JOIN geneset_membership gsm
             ON (gsm.gene_name = gene_stats.gene_name)
    ;
QUERY_TEMPLATE
)

# Cell-level expression; filtered
expr_by_aggregate_genesets=$(cat <<QUERY_TEMPLATE
    SELECT  cluster_name
           ,cell_id
           ,gene_name
           ,expr_val

      FROM expression expr

            JOIN geneset_membership gsm
           USING (gene_name)

            JOIN (SELECT    gene_name        AS gene_name
                           ,STDDEV(expr_val) AS stddev_bygene
                           ,AVG(expr_val)    AS avg_bygene
                  FROM     expression AS expr
                  GROUP BY gene_name
                 ) gene_stats
           USING (gene_name)

            JOIN clusters
           USING (cell_id)

            JOIN cluster_methods
           USING (cluster_id)

     WHERE geneset_id IN (SELECT    geneset_id
                             FROM      genesets
                                        JOIN geneset_membership
                                       USING (geneset_id)

                             GROUP BY  geneset_id
                             HAVING       (geneset_group = 'msigdb-v7.2-C7'    AND count(*) BETWEEN 190 AND 210)
                                       OR (geneset_group = 'msgidb-v7.2-C5-bp' AND count(*) BETWEEN 50  AND 100)
                            )
    ;
QUERY_TEMPLATE
)


# ------------------------------
# Functions
function helper_join_arr() {
    delim="${1:?"Please provide a join delimiter"}"
    shift

    elem_ndx=0
    for join_elem in "$@"; do
        if [[ ${elem_ndx} -eq 0 ]]; then
            echo -n "${join_elem}"
        else
            echo -n "${delim}${join_elem}"
        fi

        elem_ndx=$((elem_ndx + 1))
    done
}

function helper_parse_inputfile() {
    inputfile="${1:?"Please provide an input file to parse"}"

    elem_ndx=0
    while read -r line
    do
        [[ ${elem_ndx} -gt 0 ]] && echo -n ' OR '

        echo -n '('
        echo -n "${line}"
        echo    ')'

        elem_ndx=$((elem_ndx + 1))
    done < "${inputfile}"
}

function helper_runquery() {
    test_name="${1:?"Please provide a test name"}"
    query_str="${2:?"Please provide a query template"}"
    output_filename="${3:-"query_result.output"}"

    output_filepath="${output_basedir}/${test_name}/${output_filename}"

    # be sure not to clobber existing outputs
    if [[ ! -f "${output_filepath}" ]]; then

        # create the output directory if needed
        [[ ! -d $(dirname "${output_filepath}") ]] && mkdir -p $(dirname "${output_filepath}")

        # run the test and save the result to output file

        # first do an explain
        explain_str="EXPLAIN ANALYZE ${query_str}"
        explain_filepath="${output_filepath}.explain"

        echo -e "query string:\t\nEXPLAIN ANALYZE\n${query_str}" > "${explain_filepath}"

        psql -hlocalhost "${dbname}" -c '\pset footer off'   \
                                     -c "${explain_str}"     \
                                     >> "${explain_filepath}"

        # then do the actual query
        echo -e "query string:\t\n${query_str}" > "${output_filepath}"

        psql -hlocalhost "${dbname}" -c '\pset footer off'   \
                                     -c "${query_str}"       \
                                     >> "${output_filepath}"

    fi
}

function helper_runquery_on_inputs() {
    test_name="${1:?"Please provide a test name"}"
    query_template="${2:?"Please provide a query template"}"
    shift 2

    inputfiles=("$@")

    test_id=1
    for inputfile in "${inputfiles[@]}"; do
        # prepare the input data
        processed_inputs=$(helper_join_arr "', '" $(cat ${inputfile}))
        query_str=$(printf "${query_template}" "'${processed_inputs}'")

        # create the output directory if needed
        output_filepath="${output_basedir}/${test_name}/${test_id}.output"

        [[ -f "${output_filepath}" ]] && continue

        [[ ! -d $(dirname "${output_filepath}") ]] && mkdir -p $(dirname "${output_filepath}")

        # run the test and save the result to output file
        echo "input file: ${inputfile}"                     >  "${output_filepath}"
        echo -en "query string:\t"                          >> "${output_filepath}"
        echo "${query_str}"                                 >> "${output_filepath}"

        psql -hlocalhost ${dbname} -c '\pset footer off' \
                                   -c "${query_str}"        >> "${output_filepath}"

        test_id=$((test_id + 1))
    done
}

function helper_runquery_on_tabfiles() {
    test_name="${1:?"Please provide a test name"}"
    query_template="${2:?"Please provide a query template"}"
    shift 2

    inputfiles=("$@")

    for inputfile in "${inputfiles[@]}"; do
        testdata_name_prefix="${inputfile:29}"
        testdata_name="${testdata_name_prefix%.input}"

        # prepare the input data
        query_inputs=$(helper_parse_inputfile "${inputfile}")
        query_str=$(printf "${query_template}" "${query_inputs}")

        # create the output directory if needed
        output_filepath="${output_basedir}/${test_name}/${testdata_name}.output"

        [[ -f "${output_filepath}" ]] && continue

        [[ ! -d $(dirname "${output_filepath}") ]] && mkdir -p $(dirname "${output_filepath}")

        # run the test and save the result to output file
        echo "input file: ${inputfile}"                     >  "${output_filepath}"
        echo -en "query string:\t"                          >> "${output_filepath}"
        echo "${query_str}"                                 >> "${output_filepath}"

        psql -hlocalhost ${dbname} -c '\pset footer off' \
                                   -c "${query_str}"        >> "${output_filepath}"
    done
}


# ------------------------------
# Main

# Queries templated by single values
# celltype_inputs=($(find "${input_basedir}/5000-cells" -type f -name "celltypes.*.input"))
# helper_runquery_on_inputs "centroid_by_cluster" "${centroid_by_cluster}" ${celltype_inputs[@]}
# helper_runquery_on_inputs "expr_by_cluster"     "${expr_by_cluster}"     ${celltype_inputs[@]}

# Queries templated by a predicate
# gene_predicate_inputs=($(find "${input_basedir}/5000-cells" -type f -name "gene_predicates.*.input"))
# helper_runquery_on_tabfiles "expr_by_gene_expr" "${expr_by_gene_expr}" ${gene_predicate_inputs[@]}

# Queries that aren't templated
# helper_runquery "expr_by_filter_geneset" "${expr_by_filter_geneset}"

# For dataset "538-celltypes"
seq_depths=("100000" "500000" "1000000")
filepath_ndx=$(( 0 ))
celltype_input_filepaths=($(
    find "${input_basedir}/538-celltypes" -type f -name "celltypes.*.input" | sort
))

for celltype_input_filepath in ${celltype_input_filepaths[@]}; do
    echo "Processing input file: '${celltype_input_filepath}'"

    # for each sequencing depth
    for seq_depth in ${seq_depths[@]}; do
        echo "Processing data for sequencing depth: '${seq_depth}'"

        query_input=$(sed -r "s/([0-9_]*)/\1_${seq_depth}/g" "${celltype_input_filepath}")
        query_input=$(helper_join_arr "', '" ${query_input})
        query_str=$(printf "${expr_by_cluster}" "'${query_input}'")

        helper_runquery "expr_by_cluster" "${query_str}" "${filepath_ndx}.${seq_depth}.output"

        filepath_ndx=$(( ${filepath_ndx} + 1 ))

    done
done
