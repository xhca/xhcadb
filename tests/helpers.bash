#!/usr/bin/bash


# ------------------------------
# Functions
function helper_join_arr() {
    delim="${1:?"Please provide a join delimiter"}"

    # shift `delim` from the parameter list
    # remaining arguments are strings we will join using `delim`
    shift 1

    # The first element is treated specially (don't prepend delim)
    first_elem="${1}"
    echo -n "${first_elem}"
    shift 1

    # remaining elements are returned with delim prepended
    for join_elem in "$@"; do
        echo -n "${delim}${join_elem}"
    done
}

function helper_parse_inputfile() {
    inputfile="${1:?"Please provide an input file to parse"}"

    elem_ndx=0
    while read -r line
    do
        [[ ${elem_ndx} -gt 0 ]] && echo -n ' OR '

        echo -n '('
        echo -n "${line}"
        echo    ')'

        elem_ndx=$((elem_ndx + 1))
    done < "${inputfile}"
}

function helper_explainquery() {
    dbname="${1:?"Please provide a database name"}"
    query_str="${2:?"Please provide a query template"}"
    explain_str=$(echo -e "EXPLAIN ANALYZE\n${query_str}")

    echo -e "query string:\t\n${explain_str}"
    psql -hlocalhost "${dbname}" -c '\pset footer off' -c "${explain_str}"
}

function helper_runquery() {
    dbname="${1:?"Please provide a database name"}"
    query_str="${2:?"Please provide a query template"}"

    echo -e "query string:\t\n${query_str}"
    psql -hlocalhost "${dbname}" -c '\pset footer off' -c "${query_str}"
}

function helper_debugquery() {
    dbname="${1:?"Please provide a database name"}"
    query_str="${2:?"Please provide a query template"}"
    debug_str=$(echo -e "${query_str}\nLIMIT 10")

    echo -e "query string:\t\n${debug_str}"
    psql -hlocalhost "${dbname}" -c '\pset footer off' -c "${debug_str}"
}
