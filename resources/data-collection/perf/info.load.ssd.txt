Overview:
        Model Family:     SandForce Driven SSDs
        Device Model:     ADATA SSD S510 120GB
        User Capacity:    120,034,123,776 bytes [120 GB]
        Sector Size:      512 bytes logical/physical
        SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)

        Timings:
            Total:
                Load (using COPY) : 69.5 minutes (4168.1 s)
                Add Constraints   : 12.4 minutes ( 743.5 s)

            Per Matrix:
                Average: 40.6 s
                Min    : 29   s
                Max    : 44   s

        Data Sizes:
            Total:
                TSV size: 10.6 GiB (11077979 KiB)

            Per Matrix:
                Average: 107.1 MiB (109683.5 KiB)
                Median : 108.2 MiB (110793   KiB)
                Min    :  77   MiB (78777    KiB)
                Max    : 112.6 MiB (115273   KiB)

Hardware:
        === START OF INFORMATION SECTION ===
        Model Family:     SandForce Driven SSDs
        Device Model:     ADATA SSD S510 120GB
        Serial Number:    02309202000100000222
        LU WWN Device Id: 0 000000 000000000
        Firmware Version: 3.3.2
        User Capacity:    120,034,123,776 bytes [120 GB]
        Sector Size:      512 bytes logical/physical
        Rotation Rate:    Solid State Device
        Device is:        In smartctl database [for details use: -P show]
        ATA Version is:   ATA8-ACS, ACS-2 T13/2015-D revision 3
        SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)
        Local Time is:    Mon Oct 26 15:47:50 2020 PDT
        SMART support is: Available - device has SMART capability.
        SMART support is: Enabled


No constraints:
                             Table "public.expression"
          Column   |         Type          | Collation | Nullable | Default
        -----------+-----------------------+-----------+----------+---------
         cell_id   | character varying(32) |           | not null |
         gene_name | character varying(16) |           | not null |
         expr_val  | smallint              |           |          |
        Indexes:
            "expression_pkey" PRIMARY KEY, btree (cell_id, gene_name)


Total Time:
        Executed in   66.53 mins   fish           external
           usr time    2.29 secs  254.00 micros    2.29 secs
           sys time    0.95 secs   99.00 micros    0.95 secs

       time to load: 4168.071172 (seconds)
       time to add constraints: 743.542376 (seconds)

       Average per matrix: 40.6 s
       Min     per matrix: 29   s
       Max     per matrix: 44   s


Data Size:
        Average: 109683.5 bytes
        Median : 110793   bytes
        Min    : 78777    bytes
        Max    : 115273   bytes
